﻿$(document).ready(function() {

	//Äåéñòâèÿ ïî óìîë÷àíèþ
	$(".tab_content").hide(); //ñêðûòü âåñü êîíòåíò
	$("ul.tabs li:first").addClass("active").show(); //Àêòèâèðîâàòü ïåðâóþ âêëàäêó
	$(".tab_content:first").show(); //Ïîêàçàòü êîíòåíò ïåðâîé âêëàäêè
	
	//Ñîáûòèå ïî êëèêó
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Óäàëèòü "active" êëàññ
		$(this).addClass("active"); //Äîáàâèòü "active" äëÿ âûáðàííîé âêëàäêè
		$(".tab_content").hide(); //Ñêðûòü êîíòåíò âêëàäêè
		var activeTab = $(this).find("a").attr("href"); //Íàéòè çíà÷åíèå àòðèáóòà, ÷òîáû îïðåäåëèòü àêòèâíûé òàá + êîíòåíò
		$(activeTab).fadeIn(); //Èñ÷åçíîâåíèå àêòèâíîãî êîíòåíòà
		return false;
	});
});